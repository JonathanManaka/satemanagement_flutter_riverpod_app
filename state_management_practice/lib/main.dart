import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:state_management_practice/homePage.dart';
import 'package:state_management_practice/userAccount.dart';

//StateNotifierProvider only wants the return type of a stateNotifier class
//NB: StateNotifier provider doesnt need premitive or even none premetive datatypes
//It only wants a class that is inheriting from StateNotifier

/*NB: Give the the StateNotifierProvider return type: 
1: Class we are returing which will be the of userAccoutNotifier
2: We return the state of the class which will be userAccount
**/
final userAccountProvider =
    StateNotifierProvider<UserAccountNotifier, UserAccount>(
        (ref) => UserAccountNotifier());
void main() {
  /**Step 1 of installing flutter riverpod package
   *-Install the Riverpod flutter_hooks packages
   *-Inject ProviderScope inside the root of the app
   *-Note: The provider helps to maintain the sate of the app or offering the sate of the app
   */
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

/**
 * Step 2: Creating Providers using StateProvider
 *- Storing the state of your variable inside the 
 */
//Storing the state of userNameStateProver


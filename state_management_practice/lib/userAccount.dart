import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

@immutable
class UserAccount {
  final String name;
  final int numberOfLikes;

  const UserAccount({
    required this.name,
    required this.numberOfLikes,
  });

  UserAccount copyWith({
    String? name,
    int? numberOfLikes,
  }) {
    return UserAccount(
      name: name ?? this.name,
      numberOfLikes: numberOfLikes ?? this.numberOfLikes,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    result.addAll({'name': name});
    result.addAll({'numberOfLikes': numberOfLikes});

    return result;
  }

  factory UserAccount.fromMap(Map<String, dynamic> map) {
    return UserAccount(
      name: map['name'] ?? '',
      numberOfLikes: map['numberOfLikes']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserAccount.fromJson(String source) =>
      UserAccount.fromMap(json.decode(source));

  @override
  String toString() =>
      'UserAccount(name: $name, numberOfLikes: $numberOfLikes)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserAccount &&
        other.name == name &&
        other.numberOfLikes == numberOfLikes;
  }

  @override
  int get hashCode => name.hashCode ^ numberOfLikes.hashCode;
}

//Class that extents the state notifier and return the state of UserAccour class
class UserAccountNotifier extends StateNotifier<UserAccount> {
//Contructure
  UserAccountNotifier() : super(const UserAccount(name: '', numberOfLikes: 0));

  void updatedName(String nameVal) {
    //Copy all the properties of state but only change the name property only
    state = state.copyWith(name: nameVal);
  }

  void incrimentLikes(int previousLikes) {
    int tempPreviNumOfLikes = previousLikes;
    tempPreviNumOfLikes += 1;
    state = state.copyWith(numberOfLikes: tempPreviNumOfLikes);
  }
}

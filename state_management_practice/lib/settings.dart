import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:state_management_practice/main.dart';

class SettingPage extends ConsumerWidget {
  const SettingPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final userAcc = ref.watch(userAccountProvider);
    return Scaffold(
      appBar: AppBar(
          title: Text(
        userAcc!.name,
        style: TextStyle(fontSize: 25),
      )),
      body: Column(
        children: [
          Center(
            child: Text(
              'YOUR NUMBER OF LIKES: ${userAcc.numberOfLikes}',
              style: TextStyle(fontSize: 55),
            ),
          )
        ],
      ),
    );
  }
}

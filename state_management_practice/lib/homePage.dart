import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:state_management_practice/main.dart';
import 'package:state_management_practice/settings.dart';

/**
 * Step 3: Creating the Class that extends the ConsumerWidget
 */

void onSubmitName(WidgetRef ref, String name) {
  //The notifier give us acces to NotiferClass properties
  ref.read(userAccountProvider.notifier).updatedName(name);
}

void onIncrLikes(WidgetRef ref, likes) {
  ref.read(userAccountProvider.notifier).incrimentLikes(likes);
}

class MyHomePage extends ConsumerWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    /**
     * Note: With ref Object you can access its properties 
     * Note: With Watch propertie you can send your state/sate variable 
     *       - With Watch propertie any changes happening you will be able to read and show
     */

    final userAcc = ref.watch(userAccountProvider);
    /**
     * To make the page/entire widget tree run only when one property(of userAccount) changes we can use the select method
     */
    final selectUserAccName =
        ref.watch(userAccountProvider.select((value) => value.name));
    //Declraring TexteditingControllers for Changing userName TextField Controller
    var userNameTxtEdtContr = TextEditingController();
    return Scaffold(
        appBar: AppBar(
          title: Text('User information'),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 50,
              ),
              Text(
                'user name: ${userAcc.name}',
                style: TextStyle(fontSize: 30),
              ),
              SizedBox(
                height: 50,
              ),
              Text(
                'User Likes: ${userAcc.numberOfLikes.toString()}',
                style: TextStyle(fontSize: 30),
              ),
              SizedBox(
                height: 25,
              ),
              TextField(
                controller: userNameTxtEdtContr,
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Change User Name'),
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                  width: 200,
                  child: MaterialButton(
                    color: Colors.amber,
                    onPressed: () {
                      String tempUserName = '';
                      tempUserName = userNameTxtEdtContr.text;
                      onSubmitName(ref, tempUserName);
                    },
                    child: Text('Change user name'),
                  )),
              IconButton(
                onPressed: () {
                  onIncrLikes(ref, userAcc.numberOfLikes);
                },
                icon: Icon(
                  Icons.heart_broken_outlined,
                  size: 25,
                  color: Colors.red,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                  width: 200,
                  child: MaterialButton(
                    color: Colors.amber,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SettingPage(),
                          ));
                    },
                    child: Text('Next Page'),
                  )),
            ],
          ),
        ));
  }
}
